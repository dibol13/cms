$(document).ready(function() { 
    
    $('.keyword-tags').next('.bootstrap-tagsinput').addClass('keyword-tags');
    
    //------------- Datepicker -------------//
    
    $("#basic-datepicker").datepicker();
    
    //------------- Select 2 -------------//
    
    $('.sel-locale').select2({placeholder: 'Locale'});
    $('.sel-createdby').select2({placeholder: 'Created By'});
    $('.sel-lastupdatedby').select2({placeholder: 'Last Updated By'});
    $('.sel-more').select2({placeholder: 'More'});
    
    
    
    // ASSOCIATED FUNDS DATA SET UP
    
    var PRESELECTED_FUNDS = [
        { id: '0', text: 'Fund 1' },
        { id: '1', text: 'Fund 2' },
        { id: '2', text: 'Fund 3' },
        { id: '3', text: 'Fund 4' }
    ];

    var ALL_FUNDS = [
        { id: '0', text: 'Fund 1' },
        { id: '1', text: 'Fund 2' },
        { id: '2', text: 'Fund 3' },
        { id: '3', text: 'Fund 4' },
        { id: '4', text: 'Fund 5' },
        { id: '5', text: 'Fund 6' }
    ];
    
    //if existing rule, show pre-selected and allow adding
    $('.view-assoc-funds').select2({
        multiple: true,
        placeholder: "Search for funds",
        data: ALL_FUNDS
    }).select2('data', PRESELECTED_FUNDS);

    // ASSOCIATED DOCUMENTS DATA SET UP
    
    var PRESELECTED_DOCS = [

    ];

    var ALL_DOCS = [
        { id: '0', text: 'Doc 1' },
        { id: '1', text: 'Doc 2' },
        { id: '2', text: 'Doc 3' },
        { id: '3', text: 'Doc 4' },
        { id: '4', text: 'Doc 5' },
        { id: '5', text: 'Doc 6' }
    ];
    
    //if existing rule, show pre-selected and allow adding
    $('.view-assoc-docs').select2({
        multiple: true,
        placeholder: "Search for funds",
        data: ALL_DOCS
    }).select2('data', PRESELECTED_DOCS);
    
    
    $(".bizRule-open").on("click", function () { $('.sel-bizRule').select2("open"); });
    $("#add-assoc-fund").on("click", function () { $('.view-assoc-funds').select2("open"); });
    $("#add-assoc-doc").on("click", function () { $('.view-assoc-docs').select2("open"); });
    
    //------------- Tags -------------//
    
    // **** This mirrors the code from advanced select just for these new menus
    
    //add tags to tag div for each different menu
    
    //usability counter to display number of tags selected from menus
    var loc_count = 0;
    var created_count = 0;
    var last_count = 0;
  
    $('.sel-locale').on('change', function () { 
        //first check if clear all button was clicked and just set this to default
        if($('.clear-query').data('clicked')) {
            $(this).select2({placeholder: 'Locale'});
            $(this).select2('val', ''); 
        }
        //then if clear all wasn't clicked, then increment as needed
        else{
            var is_selected = false;
            var i=0;

            //show clear all button if there is at least one tag
            $('.clear-query').removeClass('hidden');

            //check to see if an option selected has been already tagged
            for (i=0; i<($('.query-tags').tagsinput('items').length); i++){
                if(($('.sel-locale :selected').text()) == ($('.query-tags').tagsinput('items')[i])){
                    is_selected = true;
                }   
            }

            //if it hasn't already been selected, add to the tag count and display
            if(is_selected == false){
                $('.query-tags').tagsinput('add', $('.sel-locale :selected').text());
                loc_count++; 
                $(this).select2({placeholder: 'Locale (' + loc_count + ')'});
                $(this).select2('val', '');     
            } 
            else{
                $(this).select2('val', ''); 
            } 
            
        }    
               
    });
    
    $('.sel-createdby').on('change', function () {
        //first check if clear all button was clicked and just set this to default
        if($('.clear-query').data('clicked')) {
            $(this).select2({placeholder: 'Created By'});
            $(this).select2('val', ''); 
        }
        //then if clear all wasn't clicked, then increment as needed
        else{
            var is_selected = false;
            var i=0;

            //show clear all button if there is at least one tag
            $('.clear-query').removeClass('hidden');

            //check to see if an option selected has been already tagged
            for (i=0; i<($('.query-tags').tagsinput('items').length); i++){
                if(($('.sel-createdby :selected').text()) == ($('.query-tags').tagsinput('items')[i])){
                    is_selected = true;
                }   
            }

            //if it hasn't already been selected, add to the tag count and display
            if(is_selected == false){
                $('.query-tags').tagsinput('add', $('.sel-createdby :selected').text());
                created_count++; 
                $(this).select2({placeholder: 'Created By (' + created_count + ')'});
                $(this).select2('val', '');     
            } 
            else{
                $(this).select2('val', ''); 
            } 
            
        }
    });
    
    $('.sel-lastupdatedby').on('change', function () { 
        //first check if clear all button was clicked and just set this to default
        if($('.clear-query').data('clicked')) {
            $(this).select2({placeholder: 'Last Updated By'});
            $(this).select2('val', ''); 
        }
        //then if clear all wasn't clicked, then increment as needed
        else{
            var is_selected = false;
            var i=0;

            //show clear all button if there is at least one tag
            $('.clear-query').removeClass('hidden');

            //check to see if an option selected has been already tagged
            for (i=0; i<($('.query-tags').tagsinput('items').length); i++){
                if(($('.sel-lastupdatedby :selected').text()) == ($('.query-tags').tagsinput('items')[i])){
                    is_selected = true;
                }   
            }

            //if it hasn't already been selected, add to the tag count and display
            if(is_selected == false){
                $('.query-tags').tagsinput('add', $('.sel-lastupdatedby :selected').text());
                last_count++; 
                $(this).select2({placeholder: 'Last Updated By (' + last_count + ')'});
                $(this).select2('val', '');     
            } 
            else{
                $(this).select2('val', ''); 
            } 
            
        }
    });
    
    //reset the counters, clear the placeholder to default
    $('.clear-query').click(function(){
        $('.query-tags').tagsinput('removeAll');  
        $('.clear-query').addClass('hidden');
        $(this).data('clicked', true);

        fund_count = 0;
        channel_count = 0;
        context_count = 0;
        
        $('.sel-locale').trigger('change');
        $('.sel-createdby').trigger('change');       
        $('.sel-lastupdatedby').trigger('change');
        
        $(this).data('clicked', false);

    });
    
    $('.query-tags').on('itemRemoved', function(event) {
        // event.item: contains the item
        if($(this).tagsinput('items').length == 0){
            $('.clear-query').addClass('hidden');  
        }     
    });
        
    //------------- WYSIWYG Froala -------------//
    
    $('#froala').froalaEditor({
        height:400,
        placeholderText: 'Enter component text...',
        toolbarButtons: ['undo', 'redo' , '|', 'fontFamily', '|', 'fontSize', '|', 'paragraphFormat', '|', 'bold', 'italic', 'underline', 'strikethrough', 'outdent', 'indent', 'clearFormatting', 'insertTable'],
        fontFamilySelection: true,
        fontSizeSelection: true,
        paragraphFormatSelection: true
    });  
    
    //------------- Select 2 -------------//
    
    $('.sel-category').select2({placeholder: 'Select Category'});
    
    //------------- Smooth Scroll -------------//
    
    //plugin to enhance usabilty of jumping to sections of a page
    
    $('#minfo').click(function(){
        document.querySelector('#info').scrollIntoView({ behavior: 'smooth' });
    });
    
    $('#mrule').click(function(){
        document.querySelector('#bizRule').scrollIntoView({ behavior: 'smooth' });
    });
    
    $('#mlink').click(function(){
        document.querySelector('#linkage').scrollIntoView({ behavior: 'smooth' });
    });
    
    $('#mcomm').click(function(){
        document.querySelector('#comments').scrollIntoView({ behavior: 'smooth' });
    });
    
    $('#mhist').click(function(){
        document.querySelector('#history').scrollIntoView({ behavior: 'smooth' });
    });

    
    //------------- Gritter notices -------------//
    
    //success notice
	$('#success-notice-fav').click(function() {
    
        //write the correct notification for user interaction with the favorite button    
        if($("i", this).hasClass("ion-android-star-outline")){
           $.gritter.add({
			// (string | mandatory) the heading of the notification
			title: 'Awesome!',
			// (string | mandatory) the text inside the notification
			text: 'You successfully added this component to your favorites!',
			// (int | optional) the time you want it to be alive for before fading out
			time: '',
			// (string) specify font-face icon  class for close message
			close_icon: 'l-arrows-remove s16',
			// (string) specify font-face icon class for big icon in left. if are specify image this will not show up.
			icon: 'ion-android-star',
			// (string | optional) the class name you want to apply to that specific message
			class_name: 'success-notice'
		  }); 
        }
        else{
            $.gritter.add({
			// (string | mandatory) the heading of the notification
			title: 'Done!',
			// (string | mandatory) the text inside the notification
			text: 'You have removed this component from your favorites.',
			// (int | optional) the time you want it to be alive for before fading out
			time: '',
			// (string) specify font-face icon  class for close message
			close_icon: 'l-arrows-remove s16',
			// (string) specify font-face icon class for big icon in left. if are specify image this will not show up.
			icon: 'ion-trash-a',
		  });    
        }
				
	});
    
    //success notice for updated associations
	$('.save-associations').click(function() {
        $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Great!',
        // (string | mandatory) the text inside the notification
        text: 'We updated those associations for you!',
        // (int | optional) the time you want it to be alive for before fading out
        time: '',
        // (string) specify font-face icon  class for close message
        close_icon: 'l-arrows-remove s16',
        // (string) specify font-face icon class for big icon in left. if are specify image this will not show up.
        icon: 'ion-checkmark',
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'success-notice'
      }); 	
	});
    
    //success notice for commenting
	$('.save-comment').click(function() {
        $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Awesome!',
        // (string | mandatory) the text inside the notification
        text: 'You added a comment to this component!',
        // (int | optional) the time you want it to be alive for before fading out
        time: '',
        // (string) specify font-face icon  class for close message
        close_icon: 'l-arrows-remove s16',
        // (string) specify font-face icon class for big icon in left. if are specify image this will not show up.
        icon: 'ion-chatbubble-working',
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'success-notice'
      }); 	
	});
    
    //success notice for commenting
	$('.save-owner').click(function() {
        $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Perfect!',
        // (string | mandatory) the text inside the notification
        text: 'There is now a new owner of this component!',
        // (int | optional) the time you want it to be alive for before fading out
        time: '',
        // (string) specify font-face icon  class for close message
        close_icon: 'l-arrows-remove s16',
        // (string) specify font-face icon class for big icon in left. if are specify image this will not show up.
        icon: 'ion-person-add',
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'success-notice'
      }); 	
	});
    
    //------------- Other -------------//
    
    //toggle ui if someone has favorited the component
    $('.panel-favorite').click(function() {
        $("i", this).toggleClass("ion-android-star ion-android-star-outline");
    });
    
    //hacky way to reduce editable area width and show side drawer with editable meta-data info
    $('#toggle-meta').click(function() {
        $("i", this).toggleClass("ion-chevron-left ion-chevron-right");
        $('#component-edit-area').toggleClass("col-md-11 col-md-4 component-edit-area-shift");
        $('#drawerExample2').toggleClass("drawer-shift");
        $('.drawer').drawer('toggle');
    });


    //------------- Autosize Text area -------------//
	$('.elastic').autosize();
    
//    //modal expand when want to do searching when adding
//    $('#add-assoc-doc').click(function(){
//        $('.modal-dialog').toggleClass('modal-sm modal-expand');    
//    });
//    
//    //modal reset styles when closing 
//    $('#assoc-docs').on('hidden.bs.modal', function () {
//        $('.modal-dialog').removeClass('modal-expand');
//        $('.modal-dialog').addClass('modal-sm');
//    });
    
    //------------- Masked input fields -------------//
	$("#mask-phoneExt").mask("(999) 999-9999? x99999");

});