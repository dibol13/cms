//------------- reporting.js -------------//
$(document).ready(function() {
    
    //define chart colours first
	var chartColours = {
		gray: '#bac3d2',
		teal: '#43aea8',
		blue: '#60b1cc',
		red: '#df6a78',
		orange: '#cfa448',
		gray_lighter: '#e8ecf1',
		gray_light: '#777777',
		gridColor: '#bfbfbf',
        appblue: '#216699',
        appred:'#e05b4b',
        appyellow:'#efca4d',
        appgreen:'#44b19b',
        apporange:'#e37a3e'
	}

	//convert the object to array for flot use
	var chartColoursArr = Object.keys(chartColours).map(function (key) {return chartColours[key]});
    
   

	//------------- Donut chart -------------//
	$(function () {
		var options = {
			series: {
				pie: { 
					show: true,
					innerRadius: 0.55,
					highlight: {
						opacity: 0.1
					},
					radius: 1,
					stroke: {
						width: 5
					},
					startAngle: 2.15
				}					
			},
			legend:{
				show:true,
				labelFormatter: function(label, series) {
				    return '<div style="font-weight:bold;font-size:13px;">&nbsp;'+ label +'</div>'
				},
				labelBoxBorderColor: null,
				margin: 50,
				width: 20,
				padding: 1
			},
			grid: {
	            hoverable: true,
	            clickable: true,
	        },
	        tooltip: true, //activate tooltip
			tooltipOpts: {
				content: "%s : %y.1"+"%",
				shifts: {
					x: -30,
					y: -50
				},
				theme: 'dark',
				defaultTheme: false
			}
		};
		var data = [
		    { label: "Code of Ethics",  data: 15, color: chartColours.appred},
		    { label: "Complaints",  data: 9, color: chartColours.appyellow},
            { label: "Conflicts of Interest",  data: 12, color: chartColours.appblue},
		    { label: "Corporate Compliance",  data: 2, color: chartColours.gray},
            { label: "Investment Compliance",  data: 14, color: chartColours.red},
            { label: "Market Timing",  data: 6, color: chartColours.teal},
            { label: "Overview",  data: 22, color: chartColours.apporange},
            { label: "People",  data: 8, color: chartColours.orange},
            { label: "Records Retention",  data: 12, color: chartColours.gray_lighter}
		];
	    $.plot($("#categories"), data, options);

	});
    
    //------------- Gauge -------------//

    //With percentage colors
	var gaugePercent = new Gauge(document.getElementById('used-components')).setOptions({
		lines: 10, // The number of lines to draw
		angle: 0, // The length of each line
		lineWidth: 0.4, // The line thickness
		pointer: {
			length: 0.0, // The radius of the inner circle
			strokeWidth: 0.0, // The rotation offset
			color: chartColours.gray_light // Fill color
		},
		limitMax: true,   // If true, the pointer will not go past the end of the gauge
		strokeColor: chartColours.gray_lighter,   // to see which ones work best for you
		percentColors: [[0.0, chartColours.appred ], [0.35, chartColours.apporange], [0.55, chartColours.appyellow], [1.0, chartColours.appgreen]],
		generateGradient: false	
	}); 
    // create sexy gauge!
	gaugePercent.maxValue = 100; // set max gauge value
	gaugePercent.animationSpeed = 32; // set animation speed (32 is default value)
	gaugePercent.set(80); // set actual value
    
    //Setup easy pie charts in page
    var initPieChartPage = function(lineWidth, size, animateTime, colours) {

        $(".expired-content").easyPieChart({
            barColor: colours.appgreen,
            borderColor: colours.appgreen,
            trackColor: colours.gray_lighter,
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: 5,
            size: size,
            animate: animateTime
        });

        //STATUS
        $(".status-red").easyPieChart({
            barColor: colours.appred,
            borderColor: colours.appred,
            trackColor: '#ebb7bd',
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: lineWidth,
            size: size,
            animate: animateTime
        });
        $(".status-yellow").easyPieChart({
            barColor: colours.appyellow,
            borderColor: colours.appyellow,
            trackColor: 'rgba(239,202,77,.2)',
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: lineWidth,
            size: size,
            animate: animateTime
        });
        $(".status-green").easyPieChart({
            barColor: colours.appgreen,
            borderColor: colours.appgreen,
            trackColor: '#a9e2c6',
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: lineWidth,
            size: size,
            animate: animateTime
        });
    }
    
    //------------- Morris Line chart -------------//
	var oct_data = [
		{"period": "2014-10-01", "Components": 560},
		{"period": "2014-10-02", "Components": 340},
		{"period": "2014-10-03", "Components": 326},
		{"period": "2014-10-04", "Components": 730},
		{"period": "2014-10-05", "Components": 145},
		{"period": "2014-10-06", "Components": 190},
		{"period": "2014-10-07", "Components": 459},
		{"period": "2014-10-08", "Components": 567},
		{"period": "2014-10-09", "Components": 345},
		{"period": "2014-10-10", "Components": 800}
	];
	Morris.Line({
		element: 'content-creation',
		data: oct_data,
		xkey: 'period',
		ykeys: ['Components'],
		labels: ['Total Components'],
		resize: true,
		lineColors: chartColoursArr,
		lineWidth: 2,
		pointSize: 4,
		hideHover: 'auto',
		preUnits: '',
		fillOpacity: '0.6'
	});

 //------------- Init Easy pie charts -------------//
    //pass the variables to pie chart init function
    //first is line width, size for pie, animated time , and colours object for theming.
	initPieChartPage(20,100,1500, chartColours);
	
});