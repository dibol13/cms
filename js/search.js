    /* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>'+d.body+'</td>'+
        '</tr>'+
    '</table>';
}

//------------- forms-advanced.js -------------//
$(document).ready(function() {
    
	//------------- Select 2 -------------//
    
	$('.sel-fund').select2({placeholder: 'Select Fund'});
    $('.sel-channel').select2({placeholder: 'Select Channel'});
    $('.sel-context').select2({placeholder: 'Select Context'});
    $('.sel-more').select2({placeholder: 'Other Filters'});
    $('.sel-export').select2({
        placeholder: 'Export As...',
        minimumResultsForSearch: Infinity
    });
    
    
    //------------- Tags -------------//
    
    //add tags to tag div for each different menu
    
    //usability counter to display number of tags selected from menus
    var fund_count = 0;
    var channel_count = 0;
    var context_count = 0;
  
    $('.sel-fund').on('change', function () { 
        //first check if clear all button was clicked and just set this to default
        if($('.clear-query').data('clicked')) {    
            $(this).select2({placeholder: 'Select Fund'});
            $(this).select2('val', ''); 
        }
        //then if clear all wasn't clicked, then increment as needed
        else{
            var is_selected = false;
            var i=0;

            //show clear all button if there is at least one tag
            $('.clear-query').removeClass('hidden');

            //check to see if an option selected has been already tagged
            for (i=0; i<($('.query-tags').tagsinput('items').length); i++){
                if(($('.sel-fund :selected').text()) == ($('.query-tags').tagsinput('items')[i])){
                    is_selected = true;
                }   
            }

            //if it hasn't already been selected, add to the tag count and display
            if(is_selected == false){
                $('.query-tags').tagsinput('add', $('.sel-fund :selected').text());
                fund_count++; 
                $(this).select2({placeholder: 'Select Fund (' + fund_count + ')'});
                $(this).select2('val', '');     
            } 
            else{
                $(this).select2('val', ''); 
            } 
            
        }    
               
    });
    
    $('.sel-channel').on('change', function () {
        //first check if clear all button was clicked and just set this to default
        if($('.clear-query').data('clicked')) {
            $(this).select2({placeholder: 'Select Channel'});
            $(this).select2('val', ''); 
        }
        //then if clear all wasn't clicked, then increment as needed
        else{
            var is_selected = false;
            var i=0;

            //show clear all button if there is at least one tag
            $('.clear-query').removeClass('hidden');

            //check to see if an option selected has been already tagged
            for (i=0; i<($('.query-tags').tagsinput('items').length); i++){
                if(($('.sel-channel :selected').text()) == ($('.query-tags').tagsinput('items')[i])){
                    is_selected = true;
                }   
            }

            //if it hasn't already been selected, add to the tag count and display
            if(is_selected == false){
                $('.query-tags').tagsinput('add', $('.sel-channel :selected').text());
                channel_count++; 
                $(this).select2({placeholder: 'Select Channel (' + channel_count + ')'});
                $(this).select2('val', '');     
            } 
            else{
                $(this).select2('val', ''); 
            } 
            
        }
    });
    
    $('.sel-context').on('change', function () { 
        //first check if clear all button was clicked and just set this to default
        if($('.clear-query').data('clicked')) {
            $(this).select2({placeholder: 'Select Context'});
            $(this).select2('val', ''); 
        }
        //then if clear all wasn't clicked, then increment as needed
        else{
            var is_selected = false;
            var i=0;

            //show clear all button if there is at least one tag
            $('.clear-query').removeClass('hidden');

            //check to see if an option selected has been already tagged
            for (i=0; i<($('.query-tags').tagsinput('items').length); i++){
                if(($('.sel-context :selected').text()) == ($('.query-tags').tagsinput('items')[i])){
                    is_selected = true;
                }   
            }

            //if it hasn't already been selected, add to the tag count and display
            if(is_selected == false){
                $('.query-tags').tagsinput('add', $('.sel-context :selected').text());
                context_count++; 
                $(this).select2({placeholder: 'Select Context (' + context_count + ')'});
                $(this).select2('val', '');     
            } 
            else{
                $(this).select2('val', ''); 
            } 
            
        }
    });
    
    //reset the counters, clear the placeholder to default
    $('.clear-query').click(function(){
        $('.query-tags').tagsinput('removeAll');  
        $('.clear-query').addClass('hidden');
        $(this).data('clicked', true);

        fund_count = 0;
        channel_count = 0;
        context_count = 0;
        
        $('.sel-fund').trigger('change');
        $('.sel-channel').trigger('change');       
        $('.sel-context').trigger('change');
        
        $(this).data('clicked', false);

    });
    
    $('.query-tags').on('itemRemoved', function(event) {
        // event.item: contains the item
        if($(this).tagsinput('items').length == 0){
            $('.clear-query').addClass('hidden');  
        }     
    });
    
    $('#toggle-right-sidebar').click(function(){    
        $(this).parent().parent().toggleClass("selected");  
    });
    
    //-------------- Saving Query Functions ----------------//
    
    $('#finish-save-query').click(function(){
        
        $('.un-save-query').removeClass("gray-light");
        $('.un-save-query').addClass("yellow");
        
        $('.un-save-query').html("<i class='ion-android-star mr5'></i> Saved Query");
        
        //------------- Gritter Notifications -------------//

        //success notice for saved query
        $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Nice!',
            // (string | mandatory) the text inside the notification
            text: 'You saved this query!',
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string) specify font-face icon  class for close message
            close_icon: 'l-arrows-remove s16',
            // (string) specify font-face icon class for big icon in left. if are specify image this will not show up.
            icon: 'ion-android-star',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'success-notice'
        }); 	
    });
    
    $('.un-save-query').click(function(e){ 
        //when you unsave, you will have to rename a query when you save it
        $('.text-input').val('');
        
        if($(this).hasClass("yellow")){
            //don't show save modal if un-saving query
            e.stopPropagation();
            $(this).removeClass("yellow");
            $(this).addClass("gray-light");
            

            $(this).html("<i class='ion-android-star-outline mr5'></i> Save Query");
            //remove notice for saved query
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Okay.',
                // (string | mandatory) the text inside the notification
                text: 'You unsaved this query.',
                // (int | optional) the time you want it to be alive for before fading out
                time: '',
                // (string) specify font-face icon  class for close message
                close_icon: 'l-arrows-remove s16',
                // (string) specify font-face icon class for big icon in left. if are specify image this will not show up.
                icon: 'ion-android-star-outline'
            });   
        }
    });
    
    //TABLES

    var restable = $('#searchresults').DataTable( {
        "data": [
            {
              "id": "12345",
              "title": "SMA Trading",
              "category": "Equity General",
              "owner": "Morgan Stewart",
              "expiration": "12/31/16",
              "code": "C05",
              "body": "This is the component body for this one"
            },

            {
              "id": "12368",
              "title": "CCA/Soft Dollars",
              "category": "Equity Trading",
              "owner": "Richard Wells",
              "expiration": "1/31/17",
              "code": "C89",
              "body": "This is the component body for this one which is very different than the other one"
            }
          ],
        "paging": false,
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            { "data": "id" },
            { "data": "title" },
            { "data": "category" },
            { "data": "owner" },
            { "data": "expiration" },
            { "data": "code" }
        ],
        "order": [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#searchresults tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = restable.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});