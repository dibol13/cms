$(document).ready(function() { 
    
    //------------- Select 2 -------------//
    
    $('.sel-asset-class').select2({placeholder: 'Select Asset Class'});
    $('.sel-asset-class').val('equity').trigger('change');
    
    $('.sel-status').select2({placeholder: 'Select Status'});
    $('.sel-status').val('active').trigger('change');
    
    $('.sel-board').select2({placeholder: 'Select Board Members'});
    $('.sel-board').val('trustee').trigger('change');
    
    $('.sel-entity').select2({placeholder: 'Select Entity Type'});
    $('.sel-entity').val('trust').trigger('change');
    
    $('.sel-pri').select2({placeholder: 'Select Primary Benchmark'});
    $('.sel-pri').val('russ').trigger('change');
    
    $('.sel-sec').select2({placeholder: 'Select Secondary Benchmark'});
    $('.sel-sec').val('sp').trigger('change');
    
    //init color for active vs inactive
    if($('.sel-status .select2-chosen').html()=="ACTIVE"){
        $('.sel-status .select2-chosen').css('color','#44b19b');
    }
    else{
        $('.sel-status .select2-chosen').css('color','#e05b4b');
    }
    
    //then continue to check on change to set the color correctly
    $('.sel-status').on('change', function () { 
        if($('.sel-status .select2-chosen').html()=="ACTIVE"){
            $('.sel-status .select2-chosen').css('color','#44b19b');
        }
        else{
            $('.sel-status .select2-chosen').css('color','#e05b4b');
        }  
        
    });
    
    //------------- Datepicker -------------//
    
    $("#basic-datepicker").datepicker();
    
    //------------- Table Tools -------------//
    
    $('.class-details').dataTable( {
//      "sDom":'<"top">rt<"bottom"><"clear">'
    } );
    


});